import json
import requests
import sys
import os
import getpass
import urllib
import time
import random

def clear():
    print("\n" * 100)

class api:
    def post(self, api_url, tail, data, header, files):
        self.r = requests.post(api_url + tail, data=data, headers=header, files=files)

    def upload(self, cookie):
        if not os.path.exists('geuploaded'):
            os.mkdir('geuploaded')
        with open('tags.json', 'r') as f:
            data = json.load(f)
        while True:
            num = []
            for f in pr0Filter:
                if len(os.listdir(f)) > 0:
                    num.append(f)
            num = random.choice(num)
            ran = random.randint(1, len(data[num]))
            ran -= 1
            cookie_str = "me=" + cookie["me"] + "; pp=" + cookie["pp"]
            header = {
                'Cookie': cookie_str
            }
            with open('{0}/{1}'.format(num, data[num][ran]['name']), 'rb') as f:
                img = {
                    'image': f
                }
                tags = data[num][ran]['tags']
                if num == "nsfw":
                    tags = "nsfw," + tags
                elif num == "nsfl":
                    tags = "nsfl," + tags
                self.post(api_url, "items/upload", {}, header, img)
            if self.r.status_code == 200:
                key = json.loads(self.r.text)['key']
                nonce = json.loads(urllib.parse.unquote(cookie["me"]))["id"][:16]
                data2 = {
                    'sfwstatus': num,
                    'tags': tags,
                    'checkSimilar': data[num][ran]['checkSimilar'],
                    'key': key,
                    'processAsync': 1,
                    '_nonce': nonce
                }
                self.post(api_url, "items/post", data2, header, {})
                similarCheck = json.loads(self.r.text)
                if similarCheck['error']:
                    print('Das bild {0} wurde als repost makiert. Wenn es sich um kein repost handelt, änder in der Tags Datei checkSimilar zur einer 0'.format(data[num][ran]['name']))
                    for d in similarCheck['similar']:
                        print('Dieser Post wurde als ähnlich empfunden -> https://pr0gramm.com/new/{0}'.format(d['id']))
                    if not os.path.exists('repost'):
                        os.mkdir('repost')
                    os.rename('{0}/{1}'.format(num, data[num][ran]['name']), 'repost/{0}'.format(data[num][ran]['name']))
                    time.sleep(5)
                else:
                    os.rename('{0}/{1}'.format(num, data[num][ran]['name']), 'geuploaded/{0}'.format(data[num][ran]['name']))
                del data[num][ran]
                os.remove('tags.json')
                with open('tags.json', 'w') as f:
                    json.dump(data, f, indent=4)
                if len(data['sfw']) == 0 and len(data['nsfw']) == 0 and len(data['nsfl']) == 0: 
                    break
                print('waiting....')
                sleep = random.randint(1800, 2000)
                print('Sleep time -> {0} min'.format(sleep / 60))
                time.sleep(sleep)
            else:
                print(json.loads(self.r.text)['msg'])
                input('Drücke enter')
            
    def login(self, username, password):
        self.post(api_url, "user/login", {"name": username, "password": password}, {}, {})
        if self.r.json()['success']:
            with open("cookie.json", 'w') as temp_file:
                temp_file.write(json.dumps(requests.utils.dict_from_cookiejar(self.r.cookies)))
        else:
            print("Falscher username oder Password")

def tagList(data, name, old):
    e = []
    if old != 0:
        for d in old[name]:
            for b in os.listdir(name):
                if d['tags'] != '' and d['name'] == b:
                    e.append(d['name'])
                    data[name].append(d)
    for f in os.listdir(name):
        if f in e:
            pass
        else:
            data[name].append({'name': f, 'tags': '', 'checkSimilar': 1})
    return data

def check():
    filterRange = pr0Filter
    nope = False
    with open('tags.json', 'r') as l:
        data = json.load(l)
    for f in filterRange:
        if nope == True:
            break
        if len(os.listdir(f)) != len(data[f]):
            nope = True
            break
        #if len(data[f]) == 0:
        #    nope = True
        #    break
        for b in data[f]:
            if nope == True or len(os.listdir(f)) == 0:
                nope = True
                break
            for l in os.listdir(f):
                if b['name'] == l:
                    nope = False
                    break
                else:
                    nope = True
    if nope == True:
        return False
    else:
        return True 

api_url = "https://pr0gramm.com/api/"
pr0Filter = ['sfw', 'nsfw', 'nsfl']

while True:
    try:
        if not os.path.exists("sfw") or not os.path.exists("nsfw") or not os.path.exists("nsfl"):
            for f in pr0Filter:
                try:
                    os.mkdir(f)
                except FileExistsError:
                    pass
            print("Willst du noch Bilder hinzufügen? (y/n)")
            x = input()
            if x == "y":
                clear()
                if os.path.exists('tags.json'):
                    os.remove('tags.json')
                print("Waiting ....")
                time.sleep(5)
                input("Wenn du fertig bist drück Enter")
                data = {
                    'sfw': [],
                    'nsfw': [],
                    'nsfl': []
                }
                data = tagList(data, 'sfw', 0)
                data = tagList(data, 'nsfw', 0)
                data = tagList(data, 'nsfl', 0)
                with open('tags.json', 'w') as f:
                    json.dump(data, f, indent=4)
        else:
            try:
                ueber = check()
            except FileNotFoundError:
                ueber == False
            if ueber == False:
                try:
                    with open('tags.json', 'r') as f:
                        old = json.load(f)
                    os.remove('tags.json')
                except FileNotFoundError:
                    old = 0
                    pass
                data = {
                    'sfw': [],
                    'nsfw': [],
                    'nsfl': []
                }
                data = tagList(data, 'sfw', old)
                data = tagList(data, 'nsfw', old)
                data = tagList(data, 'nsfl', old)
                with open('tags.json', 'w') as f:
                    json.dump(data, f, indent=4)
        if os.path.exists("cookie.json"):
            with open("cookie.json", "r") as cookie:
                cookie = json.load(cookie)
                first = True
                while True:
                    if first == False:
                        clear()
                        print("exit")
                        sys.exit()
                    x = input('willst du den Bot jetzt starten? (Du kannst jetzt noch die Tags hinzufügen) (y/n)')
                    if x == 'y':
                        up = api()
                        up.upload(cookie)
                        first = False
                    elif x == 'n':
                        clear()
                        print("exit")
                        sys.exit()
        else:
            print("Gib deinen Username ein")
            user = input()
            clear()
            print("Gib dein Password ein")
            password = getpass.getpass()
            clear()
            print("Login in ...")
            login = api()
            login.login(user, password)
    except KeyboardInterrupt:
        clear()
        print("exit")
        sys.exit()
